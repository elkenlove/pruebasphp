# ytlibre

Extractor de vídeos de Youtube escrito en PHP, ECMAScript, CSS y HTML.

## Capacidades
- Permite extraer vídeos de Youtube para su descarga y visualización con o sin ECMAScript (JavaScript).

## Instalación

1. Clona este repo `ytlibre` en un directorio, mediante:

        $ git clone https://libregit.org/heckyel/ytlibre

2. Cambia permisos a `ytlibre` solo si el **servidor no esta configurado**, caso contrario salta al paso 3.

        $ sudo chown $USER:http -R ytlibre/

        $ sudo chmod g+s ytlibre/

3. Asigna permisos de escritura a `templates_c/`

        $ cd ytlibre/ && mkdir templates_c/

        $ chmod g+w templates_c/

        $ mkdir embed/templates_c/

        $ chmod g+w embed/templates_c/

4. Listo

   > http:http ← usuario y grupo del servidor predeterminado en Hyperbola GNU/Linux-Libre
   >
   > www-data:www-data ← usuario y grupo del servidor predeterminado en Trisquel GNU/Linux
   >
   > `sudo chmod g+s path/` ← Asigna el "sticky bit" para el grupo (para que los archivos y directorios que se creen arrastren la propiedad del grupo `http` o `www-data`)
   >
   > `chmod g+w path/` ← se asigna permisos de escritura a un directorio ejemplo: templates_c/

## Demos

- https://ytlibre.ga
- https://www.freakspot.net/programas/ytlibre/
- https://lablibre.tuxfamily.org/programas/ytlibre/

## Hacking

Descomentar en php.ini
extension=gettext.so

## Licencia

Esta obra esta bajo la Licencia [GNU AGPLv3+](LICENSE)

## Autores

Los colaboradores de ytlibre se encuentran en el archivo [AUTHORS](AUTHORS).
