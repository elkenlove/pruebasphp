<?php
require_once('../smarty/Smarty.class.php');
$titulo="YTLibre";
$template= new Smarty();

/* ---- ---- Variables ---- ---- */
$template->assign("titulo", $titulo);

/* ---- ---- CSS ---- ---- */
$template->assign('styles', array('frond' => '../templates/css/frond.min.css',
                                  'normalize' => '../templates/css/normalize.css')
);

/* ---- ---- ---- Generated ---- ---- ---- ---- */
if (empty($_GET['link'])){
    $template->display('../templates/index.tpl');
} else {
    /* ---- ---- video-generated ---- ---- */
    require_once "../tools/processor.php";
    $video_decode = new Smarty();

    /* ---- ---- Variables ---- ---- */
    $video_decode->assign("titulo", $titulo);
    $video_decode->assign("videoTitle", $videoTitle);
    $video_decode->assign("librethumb", $librethumb);

    $video_decode->assign("videosStream", $videosStream);

    /* ---- ---- CSS ---- ----  */
    $video_decode->assign('styles', array('frond' => '../templates/css/frond.min.css',
                                          'normalize' => '../templates/css/normalize.css',
                                          'plyr' => '../templates/libs/plyr/plyr.css',
                                          'salida' => '../templates/css/salida.min.css')
    );

    /* ---- ---- JS ---- ---- */
    $video_decode->assign('javascript', array('plyr' => '../templates/libs/plyr/plyr.min.js')
    );

    $video_decode->display('../templates/embed.tpl');
}
