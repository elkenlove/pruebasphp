<?php
require_once('smarty/Smarty.class.php');
$titulo="YTLibre";
$template= new Smarty();

/* ---- ---- Variables ---- ---- */
$template->assign("titulo", $titulo);

/* ---- ---- CSS ---- ---- */

$template->assign('styles', array('frond' => 'templates/css/frond.min.css',
                                  'normalize' => 'templates/css/normalize.css')
);

/* ---- ---- ---- Generated ---- ---- ---- ---- */
if (empty($_GET['link'])){
    $template->display('templates/index.tpl');
} else {
    /* ---- ---- video-generated ---- ---- */
    require_once "tools/processor.php";
    $video_decode = new Smarty();

    /* ---- ---- Variables ---- ---- */
    $video_decode->assign("titulo", $titulo);
    $video_decode->assign("videoTitle", $videoTitle);
    $video_decode->assign("videoThumbURL", $videoThumbURL);
    $video_decode->assign("videoAuthor", $videoAuthor);
    $video_decode->assign("videoChannel", $videoChannel);
    $video_decode->assign("videoDuration", $videoDuration);
    $video_decode->assign("videoViews", $videoViews);
    $video_decode->assign("librethumb", $librethumb);

    $video_decode->assign("cStreams", $cStreams);
    $video_decode->assign("baselink", $baselink);
    $video_decode->assign("videosStream", $videosStream);

    /* ---- ---- CSS ---- ----  */
    $video_decode->assign('styles', array('frond' => 'templates/css/frond.min.css',
                                          'normalize' => 'templates/css/normalize.css',
                                          'plyr' => 'templates/libs/plyr/plyr.css',
                                          'salida' => 'templates/css/salida.min.css')
    );

    /* ---- ---- JS ---- ---- */
    $video_decode->assign('javascript', array('plyr' => 'templates/libs/plyr/plyr.min.js')
    );

    /* ---- ---- Embed ---- ---- */
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pro = 'https';
    } else {
        $pro = 'http';
    }

    $subject =  $pro."://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];

    $pattern = array();
    $pattern[0] = '/index.php/';
    $substitution = array();
    $substitution[0] = 'embed?link=';

    $embed_url = preg_replace($pattern, $substitution, $subject);

    $decode_url_embed = "<iframe width='560' height='315' src='".$embed_url.$baselink."' frameborder='0' scrolling='no'></iframe>";

    $current_url = htmlentities($decode_url_embed);

    $video_decode->assign("current_url", $current_url);
    /* ---- ---- END ---- --- */

    $video_decode->display('templates/video.tpl');
}
