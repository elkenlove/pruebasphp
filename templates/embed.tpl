<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>{$titulo} | {$videoTitle}</title>
        <link rel="icon" href="templates/images/favicon.png" sizes="192x192" />
        <!--Plyr-->
        <link href="{$styles.plyr}" rel="stylesheet"/>
        <script>
         /*
            @licstart  The following is the entire license notice for the
            JavaScript code in this page.

            Copyright (C) {$smarty.now|date_format:"%Y"} Jesús E. | <heckyel@hyperbola.info>

            The JavaScript code in this page is free software: you can
            redistribute it and/or modify it under the terms of the GNU
            General Public License (GNU GPL) as published by the Free Software
            Foundation, either version 3 of the License, or (at your option)
            any later version.  The code is distributed WITHOUT ANY WARRANTY;
            without even the implied warranty of MERCHANTABILITY or FITNESS
            FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

            As additional permission under GNU GPL version 3 section 7, you
            may distribute non-source (e.g., minimized or compacted) forms of
            that code without the copy of the GNU GPL normally required by
            section 4, provided you include this license notice and a URL
            through which recipients can access the Corresponding Source.


            @licend  The above is the entire license notice
            for the JavaScript code in this page.
          */
        </script>
    </head>
    <body>
        <video class="player-ply" controls poster="{$librethumb}" data-setup="{}">
            {foreach $videosStream as $stream}
                {$stream = json_decode(json_encode($stream))}
                <source data-res="{$stream->quality}" src="{$stream->url}" type="{$stream->libretype}"/>
            {/foreach}
            <p>Lo siento, este navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
        </video>
        <!--Plyr-->
        <script>
         // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
         document.addEventListener('DOMContentLoaded', () => {
             const players = Array.from(document.querySelectorAll('.player-ply')).map(player => new Plyr(player));
         });
         // @license-end
        </script>
        <!--EndPlyr-->
        <script src="{$javascript.plyr}" integrity="sha512-k0w6wxzlpLJ+mv/hTFFSS54ePiIgRTs+qJbGJZvCiHhmUI/gsoo9FpRnxvf1f1aWuGN2/bQ1F10Uz6GXkAFuSQ=="></script>
    </body>
</html>
