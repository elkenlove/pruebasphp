## Hacking

## SASS to CSS

1. Requeriments:

    ```
    gem install sass
    ```

2. Generate CSS from SASS:

    ```
    sass -t compressed --sourcemap=none sass/frond.sass:css/frond.min.css
    ```

    ```
    sass -t compressed --sourcemap=none sass/salida.sass:css/salida.min.css
    ```

3. Show CSS to SASS live

    ```
    sass --watch -t compressed sass/frond.sass:css/frond.min.css
    ```

    ```
    sass --watch -t compressed sass/salida.sass:css/salida.min.css
    ```
