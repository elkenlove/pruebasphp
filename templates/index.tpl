<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>{$titulo}</title>
        <link rel="icon" href="templates/images/favicon.png" sizes="192x192" />
        <link href="{$styles.normalize}" rel="stylesheet"/>
        <link href="{$styles.frond}" rel="stylesheet"/>
    </head>
    <body>
        <div class="contenedor">
            <header>
                <h2>Extractor de Youtube</h2>
            </header>
            <section class="libreyt">
                <h2>Herramienta Libre</h2>
                <hr>
                <form method="get" autocomplete="off" class="formulario">
                    <input name="link" type="text" placeholder="Enlace de Youtube"/>
                    <input class="boton" name="submit" type="submit" value="Decodificar"/>
                </form>
            </section>
            <footer>
                <p class="copyleft">Esta web es Software Libre y esta disponible en <a class="enlace" rel="noopener noreferrer"
                                                                                       href="https://libregit.org/heckyel/ytlibre.git"
                                                                                       target="_blank">LibreGit</a> bajo la Licencia
                    <a class="enlace" rel="license noopener noreferrer" href="https://www.gnu.org/licenses/agpl-3.0.html" target="_blank">
                        <abbr title="GNU Affero General Public License version 3">GNU AGPLv3+</abbr>
                    </a>
                </p>
            </footer>
        </div>
    </body>
</html>
