<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>{$titulo} | {$videoTitle}</title>
        <link rel="icon" href="templates/images/favicon.png" sizes="192x192" />
        <link href="{$styles.normalize}" rel="stylesheet"/>
        <link href="{$styles.salida}" rel="stylesheet"/>
        <!--Plyr-->
        <script>
         /*
            @licstart  The following is the entire license notice for the
            JavaScript code in this page.

            Copyright (C) {$smarty.now|date_format:"%Y"} Jesús E. | <heckyel@hyperbola.info>

            The JavaScript code in this page is free software: you can
            redistribute it and/or modify it under the terms of the GNU
            General Public License (GNU GPL) as published by the Free Software
            Foundation, either version 3 of the License, or (at your option)
            any later version.  The code is distributed WITHOUT ANY WARRANTY;
            without even the implied warranty of MERCHANTABILITY or FITNESS
            FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

            As additional permission under GNU GPL version 3 section 7, you
            may distribute non-source (e.g., minimized or compacted) forms of
            that code without the copy of the GNU GPL normally required by
            section 4, provided you include this license notice and a URL
            through which recipients can access the Corresponding Source.


            @licend  The above is the entire license notice
            for the JavaScript code in this page.
          */
        </script>
        <link href="{$styles.plyr}" rel="stylesheet"/>
        <style>
         code, kbd, samp {
             font-family: monospace, monospace;
             font-size: 0.8em;
             background: black;
             text-align: center;
             padding: 1em;
             border: solid 1px #00b7f1;
             margin-bottom: 1em;
         }
        </style>
    </head>
    <body>
        <div class="contenedor">
            <header>
                <a class="drm-free" rel="noopener noreferrer" href="https://www.defectivebydesign.org/drm-free" target="_blank"></a>
                <h2>{$videoTitle}</h2>
            </header>
            <hr>
            <section class="libreyt">
                <div class="contenedor">
                    <div class="row">
                        <div class="imageyt">
                            <img alt="{$videoTitle}" src="{$videoThumbURL}"/>
                        </div>
                        <div class="features">
                            <h6 class="yt-titulo">Canal</h6>
                            <h6><a class="enlace" href="https://invidio.us/channel/{$videoChannel}" rel="noopener noreferrer" target="_blank">{$videoAuthor}</a></h6>
                            <h6 class="yt-titulo">Duración</h6>
                            <h6>{$videoDuration}</h6>
                            <h6 class="yt-titulo">Vistas</h6>
                            <h6>{$videoViews}</h6>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="contenedor">
                    <h4>Formatos de vídeo</h4>
                    {foreach $cStreams as $stream}
                        {$stream = json_decode(json_encode($stream))}
                        <div class="row">
                            <div class="col">{$stream->type}</div>
                            <div class="col">{$stream->quality}</div>
                            <div class="col">{$stream->size}</div>
                            <div class="col">
                                <a class="boton-descarga" href="{$stream->url}" download>Descarga</a>
                            </div>
                        </div>
                    {/foreach}
                    <div class="librevideojs">
                        <video class="player-ply" controls poster="{$librethumb}" data-setup="{}">
                            {foreach $videosStream as $stream}
                                {$stream = json_decode(json_encode($stream))}
                                <source data-res="{$stream->quality}" src="{$stream->url}" type="{$stream->libretype}"/>
                            {/foreach}
                            <p>Lo siento, este navegador no soporta vídeo en HTML5. Por favor, cambia o actualiza tu navegador web</p>
                        </video>
                    </div>
                    <!--Plyr-->
                    <script>
                     document.addEventListener('DOMContentLoaded', () => {
                         const players = Array.from(document.querySelectorAll('.player-ply')).map(player => new Plyr(player));
                     });
                    </script>
                    <!--EndPlyr-->
                </div>
            </section>
            <code>{$baselink}</code>
            <code>{$current_url}</code>
            <footer>
                <p class="copyleft">Esta web es Software Libre y esta disponible en <a class="enlace"
                                                                                       rel="noopener noreferrer"
                                                                                       href="https://libregit.org/heckyel/ytlibre.git"
                                                                                       target="_blank">LibreGit</a> bajo la Licencia
                    <a class="enlace" rel="license noopener noreferrer" href="https://www.gnu.org/licenses/agpl-3.0.html" target="_blank">
                        <abbr title="GNU Affero General Public License version 3">GNU AGPLv3+</abbr>
                    </a>
                </p>
                <p class="copyleft">
                    <a class="enlace" href="templates/librejs.html" data-jslicense="1"
                       rel="license noopener noreferrer" target="_blank">Información de licencias de JavaScript.</a>
                </p>
            </footer>
        </div>
        <script src="{$javascript.plyr}" integrity="sha512-k0w6wxzlpLJ+mv/hTFFSS54ePiIgRTs+qJbGJZvCiHhmUI/gsoo9FpRnxvf1f1aWuGN2/bQ1F10Uz6GXkAFuSQ=="></script>
    </body>
</html>
