<?php
require_once "init.php";

if (empty($_GET['link'])){
    header('Location: index.php');
}

$baselink = htmlspecialchars($_GET['link']);

$yturl = 'youtu.be/';

if (strpos($baselink,$yturl) !== false){
    $link = preg_replace('~^https?://youtu\.be/([a-z\d]+)$~i', 'https://www.youtube.com/watch?v=$1', $baselink);
} else {
    $link = $baselink;
}

parse_str($link, $urlData);
$video_id = array_values($urlData)[0];

$videoFetchURL = file_get_contents("https://www.youtube.com/get_video_info?html5=1&video_id={$video_id}");
parse_str($videoFetchURL, $video_info);

$video_info = json_decode(json_encode($video_info));
if (!$video_info->status ===  "ok") {
    die("error in fetching youtube video data");
}
$videoTitle = $video_info->title;
$videoAuthor = $video_info->author;
$videoDurationSecs = $video_info->length_seconds;
$videoDuration = secToDuration($videoDurationSecs);
$videoChannel = $video_info->ucid;

// Begin_ViewCount
$api_key = 'AIzaSyB9TLn5HvT614SjzBv8ZOj3wL2tc7PM3M4'; // API KEY only view_count
$data_all = json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&id={$video_id}&key={$api_key}"));
$video_view = $data_all->items[0]->statistics->viewCount;

function bytes($a) {
    $unim = array("","K","M","G","T","P");
    $c = 0;
    while ($a>=1000) {
        $c++;
        $a = $a/1000;
    }
    return number_format($a,($c ? 2 : 0),",",".")." ".$unim[$c];
}

$videoViews = bytes($video_view);
// End_ViewCount

//change hqdefault.jpg to default.jpg for downgrading the thumbnail quality
$videoThumbURL = "https://i1.ytimg.com/vi/{$video_id}/hqdefault.jpg";
$librethumb = "https://i1.ytimg.com/vi/{$video_id}/maxresdefault.jpg";

if (!isset($video_info->url_encoded_fmt_stream_map)) {
    die('No data found');
}

$streamFormats = explode(",", $video_info->url_encoded_fmt_stream_map);

if (isset($video_info->adaptive_fmts)) {
    $streamSFormats = explode(",", $video_info->adaptive_fmts);
    $pStreams = parseStream($streamSFormats);
}

$cStreams = parseStream($streamFormats);

// Aquí selecciona solo 2 vídeos principales.
$videosStream = array_slice($cStreams, 0, 2);
